#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import sys
from matplotlib.animation import FuncAnimation

xlim = 3
Ns = int(sys.argv[1])
xs = np.linspace(-xlim,xlim,Ns)
w = 1.0
m = 1.0
g2 = 10.0
g4 = 0.0
gamma = 0.3

dx = 2*xlim/Ns
D = np.zeros((Ns,Ns))
D[0,1]=1
D[0,0]=-2
for i in range(1,Ns-1):
    D[i,i+1] = 1
    D[i,i] = -2
    D[i,i-1] = 1
D[Ns-1,Ns-1] = -2
D[Ns-1,Ns-2] = 1

D = D/dx**2


h0 = np.zeros((Ns,Ns))*1j
h0 += -0.5/m*D
h0 += np.diag(m*w**2*(xs**2))

h0evals, h0evecs = np.linalg.eigh(h0)
gs = np.abs(h0evecs[:,0])**2

#fig = plt.figure()
#fig, ax = plt.subplots()
plt.plot(xs, gs/dx, color='k', label='HO ground')

psi = h0evecs[:,0]

for i in range(1,41):
    h = np.zeros((Ns,Ns))*1j
    h += h0
    psix = np.zeros((Ns,Ns))*1j
    for j in range(Ns):
        psix[j,j] = np.abs(psi[j])**2

    h += g2 * psix / dx
    hevals, hevecs = np.linalg.eigh(h)
    psip = hevecs[:,0]
    if True:
        h = np.zeros((Ns,Ns))*1j
        h += h0
        psipx = np.zeros((Ns,Ns))*1j
        for j in range(Ns):
           psipx[j,j] = np.abs(psip[j])**2
        h += g2*gamma*psipx/dx + g2*(1-gamma)*psix/dx
        hevals, hevecs = np.linalg.eigh(h)
        psi = hevecs[:,0]

    if False:
        psinew = psip + (1-gamma) * psi
        psi = psinew / np.sqrt(np.sum(np.abs(psinew)**2))

    if False:
        psi = psip

    print(f'{i} {hevals[0]}')
    if i > 10:
        plt.plot(xs, np.abs(psi)**2/dx, label=f'{i}th, E={hevals[0]:.5f}')


    #ax.clear()
    #ax.plot(xs, gs/dx, color='k', label='HO ground')
    #ax.plot(xs, np.abs(psi)**2/dx, color='r', label=f'after {i}th iteration') 
    #ax.set_ylim(0,1.0)
    #ax.set_xlabel('$x$')
    #ax.set_ylabel('$|\\psi(x)|^2$')
    #ax.set_title('ground state')
    #ax.legend(loc='upper right')

plt.xlabel('$x$')
plt.ylabel('$|\\psi(x)|^2$')
plt.xlim(-3,4)
plt.ylim(0,1.1)
plt.legend(loc='upper right')
plt.title(f'g2={g2:.1f}, dx={dx:.3f}')
plt.show()



